# doping.sh
This is a little script I made to ping all the DigitalOcean servers around the world to check which has the lowest latency.

First add execute permissions:
`chmod +x doping.sh`

Run it:
`./doping.sh`